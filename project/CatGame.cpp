#include "CatGame.h"

ProgramInfo GetProgramInfo()
{
    ProgramInfo inf;
    
    inf.szScreenRez = Size(600, 400);
    inf.strTitle = "Cat Game";
    inf.nFramerate = 30;
    
    return inf;
}

SP<GlobalController> GetGlobalController(ProgramEngine pe)
{
    return new CatGameGlobalController(pe);
}

int CharToInt(char c)
{
    return int(c) - int('0');
}



Point GetInCenter(Rectangle r, Image* pImg)
{
    Point p(r.p.x + r.sz.x/2, r.p.y + r.sz.y/2);
    p.x -= pImg->GetSize().x/2;
    p.y -= pImg->GetSize().y/2;

    return p;
}

bool bSecond = false;

void Draw(SP<Graphic> pGraph, Rectangle sBound, Rectangle sBnd, Rectangle sLife, int nLife, double d = 1)
{
    Rectangle newLife(sLife);
    newLife.sz.x *= double(nLife)/100;
    
    pGraph->DrawRectangle(sBound, Color(int(d*25), int(d*25), int(d * 25)), false);
    pGraph->DrawRectangle(sBnd, Color(int(d*200), int(d*200), int(d*200)), false);
    pGraph->DrawRectangle(sLife, Color(0, 0, 0), false);
    if(!bSecond)
        pGraph->DrawRectangle(newLife, Color(int(d*255), 0, 0), false);
    else
        pGraph->DrawRectangle(newLife, Color(0, 0, int(d*255)), false);

	pGraph->RefreshAll();
}

CatGameGlobalController::CatGameGlobalController(ProgramEngine pe)
{
    ProgramInfo inf = GetProgramInfo();

    pGr = pe.pGr;

    pMsg = pe.pMsg;

    {
        std::ifstream ifs("config.txt");

        if(ifs.fail())
            throw SimpleException("CatGameGlobalController", "<constructor>", "Need config.txt file!");

        ifs >> fp;
    }
    

    pSndMng = pe.pSndMng;

    pPr = new MyPreloader(pGr, pSndMng, fp);
    MyPreloader& pr = *pPr;

    pr.LoadTS("C1.bmp", "C1");
    pr.LoadTS("C2.bmp", "C2");
    pr.LoadTS("C3.bmp", "C3");

    pSndPr = new Preloader<char>(pGr, pSndMng, fp);
    Preloader<char>& prs = *pSndPr;

    prs.LoadSnd("00_GGGG.wav", '_');
    prs.LoadSnd("00_gg.wav",   '.');
    prs.LoadSnd("00_G.wav",    'g');
    prs.LoadSnd("01_A.wav",    'A');
    prs.LoadSnd("01_B.wav",    'B');
    prs.LoadSnd("01_C.wav",    'C');
    prs.LoadSnd("01_D.wav",    'D');
    prs.LoadSnd("01_E.wav",    'E');
    prs.LoadSnd("01_F.wav",    'F');
    prs.LoadSnd("01_G.wav",    'G');


    pSndPr2 = new Preloader<char>(pGr, pSndMng, fp);
    Preloader<char>& prs2 = *pSndPr2;

    prs2.LoadSnd("T\\C.wav",   'C');
    prs2.LoadSnd("T\\G.wav",   'G');
    prs2.LoadSnd("T\\D.wav",   'D');
    prs2.LoadSnd("T\\D#.wav",  'd');
    prs2.LoadSnd("T\\F.wav",   'F');
    prs2.LoadSnd("T\\G+.wav",  'g');
    prs2.LoadSnd("T\\G#+.wav", 'h');
    prs2.LoadSnd("T\\A#.wav",  'A');
    prs2.LoadSnd("T\\C+.wav",  'c');


    sBound = inf.szScreenRez;

    sLife = Rectangle(sBound.Left() + sBound.sz.x / 4, sBound.Top() + sBound.sz.y * 3/9, sBound.Left() + sBound.sz.x * 3/4, sBound.Top() + sBound.sz.y * 4/9);
    sBnd = sLife;
    
    sBnd.p.x -= 2;
    sBnd.p.y -= 2;
    sBnd.sz.x += 4;
    sBnd.sz.y += 4;
    
    nCounter = 0;
    nRyCounter = 0;
    bLose = false;
    bPic = true;
    bCountIt = false;
    bSecond = false;
    nLoseCounter = 10;
    nLoseCounter2 = 25;
    sMelody = "CEGECEGEACECACECgBDBgBDBg.g......CEGECEGEBDFDBDFDACECACEC_";
    sRythm  = "2222213222222132222221322121111112222213222222132222221324";

    fav = ForgetfulAverage(8);

    nLife = 100;

    nTimeTake = 150;

    nTimer = GetTicks();

    Draw(pGr, sBound, sBnd, sLife, nLife);
}

void CatGameGlobalController::Update()
{
    if(!bLose)
        return;

    if(GetTicks() - nTimer >= nTimeTake - 5)
    {
        nTimer = GetTicks();
    }
    else
    {
        return;
    }
            
    if(nLoseCounter > 0)
    {
        --nLoseCounter;
        Draw(pGr, sBound, sBnd, sLife, nLife, double(nLoseCounter)/10);
    }
    
    if(!nLoseCounter && nCounter != sMelody.size())
    {
        if(nRyCounter == 0)
        {
            nRyCounter = CharToInt(sRythm[nCounter]);
            
            if(nTimeTake == 150)
                pSndMng->PlaySound( (*pSndPr).GetSnd(sMelody[nCounter++]) );
            else
                pSndMng->PlaySound( (*pSndPr2).GetSnd(sMelody[nCounter++]) );

            if(bPic)
                pGr->DrawImage(GetInCenter(sBound, pGr->GetImage((*pPr)["C1"])), (*pPr)["C1"], pGr->GetImage((*pPr)["C1"])->GetSize(), false);
            else
                pGr->DrawImage(GetInCenter(sBound, pGr->GetImage((*pPr)["C2"])), (*pPr)["C2"], pGr->GetImage((*pPr)["C2"])->GetSize(), false);
            bPic = !bPic;
		
			pGr->RefreshAll();
        }
        else
        {
            --nRyCounter;
        }
    }

    if(nCounter == sMelody.size())
    {
        if(nLoseCounter2 == 0)
        {
            if(bCountIt && !bSecond)
            {
                bSecond = true;

                sMelody = "CGDGdGCGDGdGFGDGAGgGhGFGgGdGFGDGCGDGdGCGDGdGFGDGAGgGhGFGgGdGFGDGCGDGdGCGDGdGFGDGAGgGhGFGgGdGFGDGCGDGdGCGDGdGFGDGAGgGhGFGgGdGFGDGCGDGdGCGDGdGFGDGAGgGhGFGgGdGFGDGCGDGdGCGDGdGFGDGAGgGhGFGgGdGFGDGCGDGdGCGDGdGFGDGAGgGhGFGgGdGFGDGCGDGdGCGDGdGFGDGAgdCGCdgAcAgdCdg";
                sRythm  = "2222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222";

                nLife = 100;
                nCounter = 0;
                nRyCounter = 0;
                bLose = false;
                bPic = true;
                bCountIt = false;
                nLoseCounter = 10;
                nLoseCounter2 = 25;
                avc = AverageCal();
                nTimer = GetTicks();
                nTimeTake = 30;

                Draw(pGr, sBound, sBnd, sLife, nLife);
            }
            else
            {
                nCounter = 0;
                nRyCounter = 0;
                bLose = false;
                bPic = true;
                bCountIt = false;
                bSecond = false;
                nLoseCounter = 10;
                nLoseCounter2 = 25;
                sMelody = "CEGECEGEACECACECgBDBgBDBg.g......CEGECEGEBDFDBDFDACECACEC_";
                sRythm  = "2222213222222132222221322121111112222213222222132222221324";

                avc = AverageCal();
                fav = ForgetfulAverage (8);

                nLife = 100;

                nTimeTake = 150;
                nTimer = GetTicks();

                Draw(pGr, sBound, sBnd, sLife, nLife);
            }
        }
        else
        {
            --nLoseCounter2;
            if(nLoseCounter2 < 10)
			{
                pGr->DrawImage(GetInCenter(sBound, pGr->GetImage((*pPr)["C3"])), (*pPr)["C3"], pGr->GetImage((*pPr)["C3"])->GetSize(), false);
				pGr->RefreshAll();
			}
        }
    }

}

void CatGameGlobalController::KeyDown(GuiKeyType nCode)
{
    if(nCounter == sMelody.size() && !bLose)
    {
        bCountIt = true;
        bLose = true;
        nTimer = GetTicks();
        nCounter = 0;
    }

    if(nCounter != sMelody.size() && !bLose)
    {
        int nInterval = GetTicks() - nTimer;
        nTimer = GetTicks();

        //std::cout << nInterval << "\n";

        if(nCounter != 0)
        {
            int nRyCounter = CharToInt(sRythm[nCounter - 1]);
            
            avc.NewSample(nInterval, nRyCounter);
            fav.NewSample(nInterval, nRyCounter);

            double dAvg = avc.GetAverage(nRyCounter);
            double dErr = (nInterval - dAvg)/dAvg;
            
            double dMsr = std::abs(dErr);
            
            if(!bSecond)
            {
                if(dMsr > .5)
                {
                    nLife -= 20;

                    std::cout << "\t\t";
                }
                else if(dMsr > .15)
                {
                    nLife -= 7;
                    
                    std::cout << "\t";
                }
                else if(dMsr > .05)
                {
                    nLife -= 2;
                }
            }
            else
            {
                if(nInterval > 100)
                    nLife -= (nInterval - 100)/30;
                if(nInterval < 10)
                    nLife -= 5;
            }


            if(nLife < 0)
            {
                nLife = 0;
                bLose = true;
                nTimer = GetTicks();
                nCounter = 0;
            }
            
            
            //std::cout << "Avg: " << dAvg << "\n";
            //std::cout << "Err: " << int(dErr * 100) << "% \n";
         }
        
        
        if(nTimeTake == 150)
            pSndMng->PlaySound( (*pSndPr).GetSnd(sMelody[nCounter++]) );
        else
            pSndMng->PlaySound( (*pSndPr2).GetSnd(sMelody[nCounter++]) );
        
        Draw(pGr, sBound, sBnd, sLife, nLife);

        if(bLose)
            nCounter = 0;
    }
}


