#include "Global.h"
#include "GuiGen.h"

typedef CameraControl<Index> MyCamera;
typedef GraphicalInterface<Index> Graphic;
typedef SoundInterface<Index> SoundManager;

struct ForgetfulAverage
{
    int n;
    
    int nAverage;
    int nSample;

    std::list<std::pair<int, int> > vSamples;

    ForgetfulAverage(int n_ = 5):n(n_), nAverage(0), nSample(0){}

    double GetAverage(int nW = 1)
    {
        return double(nAverage * nW)/nSample;
    }
    
    void NewSample(int nS, int nW = 1)
    {
        vSamples.push_back(std::pair<int, int>(nS, nW));
        
        nAverage += nS;
        nSample += nW;
        
        if(vSamples.size() > n)
        {
            nSample -= vSamples.front().second;
            nAverage -= vSamples.front().first;
            vSamples.pop_front();
        }
    }
};

struct AverageCal
{
    int nAverage;
    int nSample;

    AverageCal():nAverage(0), nSample(0){}

    double GetAverage(int nW = 1)
    {
        return double(nAverage * nW)/nSample;
    }
    
    void NewSample(int nS, int nW = 1)
    {
        nAverage += nS;
        nSample += nW;
    }
};

class CatGameGlobalController: public GlobalController
{
    SP<Graphic> pGr;
    SP<SoundManager> pSndMng;
    SP<MessageWriter> pMsg;

    FilePath fp;
    SP<MyPreloader> pPr;
    SP<Preloader<char> > pSndPr;
    SP<Preloader<char> > pSndPr2;

    Rectangle sBound;

    Rectangle sLife;
    Rectangle sBnd;

    int nCounter;
    int nRyCounter;
    bool bLose;
    bool bPic;
    bool bCountIt;
    int nLoseCounter;
    int nLoseCounter2;
    std::string sMelody;
    std::string sRythm;

    AverageCal avc;
    ForgetfulAverage fav;

    int nLife;

    int nTimeTake;

    unsigned nTimer;

public:

    CatGameGlobalController(ProgramEngine pe);
    ~CatGameGlobalController(){}

    /*virtual*/ void Update();
    
    /*virtual*/ void KeyDown(GuiKeyType nCode);
    /*virtual*/ void KeyUp(GuiKeyType nCode){}
};


using namespace std;